# Lava RPM

This repository contains the necessary files to build LAVA and its components into RPMs.

## License

[GPL-2.0-or-later](./COPYING)
