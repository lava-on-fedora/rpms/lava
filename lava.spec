%global lava_version 2023.02
%global lava_version_py 2023.2

Name:           lava
Version:        %{lava_version}
Release:        1
Summary:        LAVA is a continuous integration system
 
License:        GPL-2.0-or-later
URL:            https://git.lavasoftware.org/lava/lava.git
Source:         https://git.lavasoftware.org/lava/lava/-/archive/%{lava_version}/lava-%{lava_version}.tar.gz
 
BuildArch:      noarch

BuildRequires: python3-devel
BuildRequires: python3-aiohttp
BuildRequires: python3-asgiref
BuildRequires: python3-celery
BuildRequires: python3-django-auth-ldap
BuildRequires: python3-defusedxml
BuildRequires: python3-django-allauth
BuildRequires: python3-django-tables2
BuildRequires: python3-django
BuildRequires: python3-docutils
BuildRequires: python3-eventlet
BuildRequires: python3-jinja2
BuildRequires: python3-psycopg2
BuildRequires: python3-zmq
BuildRequires: python3-requests
BuildRequires: python3-simplejson
BuildRequires: python3-voluptuous
BuildRequires: python3-whitenoise
BuildRequires: python3-magic
BuildRequires: python3-configobj
BuildRequires: python3-netifaces
BuildRequires: python3-pexpect
BuildRequires: python3-pyudev
BuildRequires: python3-pytest-django
BuildRequires: python3-pytest-mock
BuildRequires: python3-pytest
BuildRequires: python3-pytest-cov
BuildRequires: python3-pymongo
BuildRequires: python3-setproctitle
BuildRequires: bmap-tools
BuildRequires: git
# BuildRequires: schroot
BuildRequires: lxc
BuildRequires: minicom
BuildRequires: telnet
BuildRequires: qemu-system-x86
BuildRequires: qemu-system-arm

%description
LAVA is an automated validation architecture primarily aimed at testing
deployments of systems based around the Linux kernel on ARM devices,
specifically ARMv7 and later.


%package python3-common
Summary: TODO
Requires: python3-pyyaml
Requires: python3-voluptuous

%description python3-common
TODO


%package python3-dispatcher
Summary: TODO
Requires: lava-python3-common
Requires: python3-aiohttp
Requires: python3-configobj
Requires: python3-magic
Requires: python3-jinja2
Requires: python3-netifaces
Requires: python3-pexpect
Requires: python3-pytest
Requires: python3-pytest-mock
Requires: python3-pyudev
Requires: python3-requests
Requires: python3-setproctitle
Requires: bmap-tools
Requires: python3-voluptuous
Requires: git
Requires: schroot
Requires: lxc
Requires: minicom
Requires: telnet
Requires: qemu-system-x86
Requires: qemu-system-arm

%description python3-dispatcher
TODO

%package dispatcher-host
Summary: TODO
Requires: lava-python3-common
Requires: python3-requests
Requires: python3-pyudev

%description dispatcher-host
TODO

%package server
Summary: TODO
Requires: lava-python3-common
Requires: python3-aiohttp
Requires: python3-asgiref
Requires: python3-celery
Requires: python3-django-auth-ldap
Requires: python3-defusedxml
Requires: python3-django-allauth
Requires: python3-django-tables2
Requires: python3-django
Requires: python3-docutils
Requires: python3-eventlet
Requires: python3-jinja2
Requires: python3-psycopg2
Requires: python3-zmq
Requires: python3-requests
Requires: python3-simplejson
Requires: python3-voluptuous
Requires: python3-whitenoise
Requires: python3-magic
Requires: python3-configobj
Requires: python3-netifaces
Requires: python3-pexpect
Requires: python3-pyudev
Requires: python3-pytest-django
Requires: python3-pytest-mock
Requires: python3-pytest
Requires: python3-pytest-cov
Requires: python3-pymongo

%description server
TODO

%package python3-linaro-django-xmlrpc
SUMMARY: TODO
Requires: python3-django
Requires: lava-server

%description python3-linaro-django-xmlrpc
TODO

%package scheduler-app
SUMMARY: TODO
Requires: python3-django
Requires: python3-yaml
Requires: python3-jinja2
Requires: python3-django-auth-ldap
Requires: python3-voluptuous
Requires: python3-django-tables2
Requires: python3-celery
Requires: python3-requests
Requires: python3-simplejson
Requires: python3-zmq
Requires: python3-pymongo
Requires: python3-google-cloud-firestore
Requires: python3-lava-common
Requires: lava-server
Requires: python3-linaro-django-xmlrpc
Requires: lava-results-app

%description scheduler-app
TODO

%package rest-app
SUMMARY: todo
Requires: python3-voluptuous
Requires: python3-yaml
Requires: python3-jinja2
Requires: python3-django
Requires: python3-django-rest-framework
Requires: python3-junit_xml
Requires: ptython3-linaro-django-xmlrpc
Requires: lava-python3-common
Requires: lava-server
Requires: lava-scheduler-app
Requires: lava-results-app
# Requires: python3-tap
# Requires: python3-django-rest-framework-extensions

%description rest-app
TODO

%package results-app
SUMMARY: TODO
Requires: python3-yaml
Requires: python3-django
Requires: python3-django-tables2
Requires: python3-lava-common
Requires: python3-linaro-django-xmlrpc
Requires: python3-simplejson
Requires: lava-server
Requires: lava-scheduler-app

%description results-app
TODO

%prep
%autosetup

%build
%py3_build

%install
%py3_install

%files
%doc README.md
%license COPYING
%{python3_sitelib}/lava/
%{python3_sitelib}/lava-%{lava_version_py}-py%{python3_version}.egg-info/PKG-INFO
%{python3_sitelib}/lava-%{lava_version_py}-py%{python3_version}.egg-info/SOURCES.txt
%{python3_sitelib}/lava-%{lava_version_py}-py%{python3_version}.egg-info/dependency_links.txt
%{python3_sitelib}/lava-%{lava_version_py}-py%{python3_version}.egg-info/not-zip-safe
%{python3_sitelib}/lava-%{lava_version_py}-py%{python3_version}.egg-info/top_level.txt
/etc/lava-coordinator/lava-coordinator
/etc/lava-coordinator/lava-coordinator.conf
/etc/logrotate.d/lava-coordinator-log
/lib/systemd/system/lava-coordinator.service
%{_bindir}/lava-coordinator
%{_bindir}/lava-outerr
%{_bindir}/lava-run
%{_bindir}/lava-worker
%exclude /etc/systemd/system/systemd-udevd.service.d/override.conf

%files python3-common
%{python3_sitelib}/lava_common/
%{_datadir}/lava-common/lava-schema.py

%files python3-dispatcher
%{python3_sitelib}/lava_dispatcher/
%{_sysconfdir}/lava-dispatcher/lava-worker
%{_sysconfdir}/exports.d/lava-dispatcher-nfs.exports
%{_datadir}/lava-dispatcher/apache2/lava-dispatcher.conf
%{_datadir}/lava-dispatcher/tftpd-hpa

%files dispatcher-host
%{python3_sitelib}/lava_dispatcher_host/
%{_sysconfdir}/lava-dispatcher/lava-worker
%{_sysconfdir}/exports.d/lava-dispatcher-nfs.exports
%{_datadir}/lava-dispatcher/apache2/lava-dispatcher.conf
%{_datadir}/lava-dispatcher/tftpd-hpa
%{_sysconfdir}/lava-dispatcher-host/lava-docker-worker
%{_sysconfdir}/logrotate.d/lava-dispatcher-host-log
/lib/systemd/system/lava-dispatcher-host.service
/lib/systemd/system/lava-dispatcher-host.socket
%{_bindir}/lava-dispatcher-host
%{_bindir}/lava-dispatcher-host-server
%{_sysconfdir}/logrotate.d/lava-docker-worker-log
/lib/systemd/system/lava-docker-worker.service
%{_bindir}/lava-docker-worker
/lib/systemd/system/lava-worker.service
%{_sysconfdir}/logrotate.d/lava-worker-log
/usr/share/lava-server/dispatcher.yaml

%files server
%{python3_sitelib}/lava_server/
/usr/bin/lava-server
/lib/systemd/system/lava-server-gunicorn.service
/etc/logrotate.d/lava-server-gunicorn-log
/etc/lava-server/env.yaml
/etc/lava-server/lava-celery-worker
/etc/lava-server/lava-publisher
/etc/lava-server/lava-scheduler
/etc/lava-server/lava-server-gunicorn
/etc/apache2/sites-available/lava-server.conf
/usr/share/lava-server/dispatcher.yaml
/usr/share/lava-server/postinst.py
/usr/share/lava-server/device-types/
/etc/logrotate.d/django-log
/lib/systemd/system/lava-publisher.service
/etc/logrotate.d/lava-publisher-log
/etc/modprobe.d/lava-modules.conf
/lib/systemd/system/lava-celery-worker.service
/etc/logrotate.d/lava-celery-worker-log

%files python3-linaro-django-xmlrpc
%{python3_sitelib}/linaro_django_xmlrpc/

%files scheduler-app
%{python3_sitelib}/lava_scheduler_app/
/lib/systemd/system/lava-scheduler.service
/etc/logrotate.d/lava-scheduler-log

%files rest-app
%{python3_sitelib}/lava_rest_app/
/etc/logrotate.d/django-log

%files results-app
%{python3_sitelib}/lava_results_app/
/etc/logrotate.d/django-log

%changelog
* Fri Jun 30 2023 Leonardo Rossetti <lrossett@redhat.com> - 202302-1
- Initital spec file
